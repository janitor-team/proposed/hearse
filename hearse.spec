# $Id: hearse.spec,v 1.7 2005-03-13 14:35:15 roderick Exp $

%define name	hearse
%define version	1.5
%define release	1
%define prefix	%{_prefix}
%define confdir	/etc

Summary:	exchange Nethack bones files with other players
Name:		%{name}
Version:	%{version}
Release:	%{release}
Prefixes:	%{prefix} %{confdir}
Copyright:	GPL
Group:		Amusements/Games
Source:		http://www.argon.org/~roderick/hearse/%{name}-%{version}.tar.gz
URL:		http://www.argon.org/~roderick/hearse/
Packager:	Roderick Schertler <roderick@argon.org>

BuildArch:	noarch
BuildRoot:	%{_builddir}/%{name}-root

# I can actually use either Digest::MD5 or MD5, but I can't see how to
# tell rpm that.
#
# I'd tried to set this requirement using $RPM_requires inside the
# script, which /usr/lib/rpm/perl.req claims to support.  They must not
# have tested that feature at all, because a typo prevents it from
# working.

Requires:	perl(Digest::MD5)

%description
Nethack sometimes saves the level on which you die (including your
stuff, what killed you, and your ghost) in a "bones file".  These files
get loaded into later Nethack games.  If you're the only Nethack player
on your system you'll only get bones files you created yourself.

With Hearse, you can automatically exchange bones files with other
Nethack players.  When run it uploads any new bones files it finds
on your system, then downloads any bones files the server feels like
giving it.  See http://www.argon.org/~roderick/hearse/ for more
information.

%prep
%setup

%build
perl Makefile.PL \
    LIB=$RPM_BUILD_ROOT/none/such \
    INSTALLMAN1DIR=$RPM_BUILD_ROOT%{prefix}/share/man/man1 \
    INSTALLMAN3DIR=$RPM_BUILD_ROOT%{prefix}/share/man/man3 \
    INSTALLBIN=$RPM_BUILD_ROOT%{prefix}/bin \
    INSTALLSCRIPT=$RPM_BUILD_ROOT%{prefix}/bin
make

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make pure_perl_install
mkdir -p $RPM_BUILD_ROOT%{confdir}/cron.d
mkdir -p $RPM_BUILD_ROOT%{confdir}/cron.daily
mkdir -p $RPM_BUILD_ROOT%{confdir}/nethack
cp debian/hearse.cron.d		$RPM_BUILD_ROOT%{confdir}/cron.d/hearse
cp debian/hearse.cron.daily	$RPM_BUILD_ROOT%{confdir}/cron.daily/hearse
cp hearse.conf			$RPM_BUILD_ROOT%{confdir}/nethack

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc Notes
%doc README*
%doc debian/changelog
%config %{confdir}/cron.d/hearse
%config %{confdir}/cron.daily/hearse
%config %{confdir}/nethack/hearse.conf
%{prefix}/bin/bones-info
%{prefix}/bin/hearse
%{prefix}/share/man/man1/bones-info.1.gz
%{prefix}/share/man/man1/hearse.1.gz
