# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: hearse\n"
"Report-Msgid-Bugs-To: hearse@packages.debian.org\n"
"POT-Creation-Date: 2009-12-22 08:53-0500\n"
"PO-Revision-Date: 2007-05-18 15:36+0100\n"
"Last-Translator: Bart Cornelis <cobaco@skolelinux.no>\n"
"Language-Team: debian-l10n-dutch <debian-l10n-dutch@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Dutch\n"

#. Type: string
#. Description
#: ../hearse.templates:1001
msgid "Email address to submit to the Hearse server:"
msgstr "Aan de Hearse-server door te geven e-mailadres:"

#. Type: string
#. Description
#: ../hearse.templates:1001
msgid ""
"The Hearse server requires that you supply an email address before you can "
"exchange bones files.  If you supply an address here it will be submitted to "
"the server for you.  If you don't supply an address, hearse will be "
"installed but it won't run automatically until you create an account "
"yourself."
msgstr ""
"De Hearse-server vereist dat u een e-mailadres opgeeft voordat u 'bones'-"
"bestanden kunt uitwisselen. Als u hier een adres opgeeft wordt dit aan de "
"server doorgegeven. Als u hier geen adres opgeeft wordt hearse wel "
"geïnstalleerd, maar zal pas automatisch werken eens u een account voor uzelf "
"heeft aangemaakt."

#. Type: string
#. Description
#: ../hearse.templates:1001
msgid ""
"The server operator states that your email address will only be used to "
"contact you about Hearse, and will never be given to any third party.  If "
"you enter an invalid address, the server won't be able to support you if you "
"download a bad bones file, and will be forced to ban you if any of your "
"uploaded files are bad."
msgstr ""
"De server-operator geeft aan dat uw e-mailadres enkel gebruikt zal worden om "
"u te contacteren voor Hearse zelf. Uw adres wordt nooit aan een derde partij "
"doorgegeven. Als u een ongeldig adres opgeeft kan de server u niet "
"ondersteunen wanneer u een slecht 'bones'-bestand ophaalt, als u hierdoor "
"slechte bestanden doorstuurt zal de server gedwongen zijn u te bannen."
